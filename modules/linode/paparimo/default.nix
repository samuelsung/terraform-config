{ inputs }:

{
  data.sops_file.paparimo-age-img = {
    source_file = "${./paparimo-age.linode.img.gz.base64}";
    input_type = "raw";
  };

  resource.local_sensitive_file.paparimo-age-img = {
    content_base64 = "\${data.sops_file.paparimo-age-img.raw}";
    filename = "\${path.module}/.secrets/paparimo.age.linode.img.gz";
  };

  resource.linode_image.paparimo-age-img = {
    label = "paparimo-age-img";
    description = ''
      Image contain the age key for sops.
      Public key: age15es8mfd20hpfd0h3hnnh208gaz32tdgahammd6f9r4g65gq4jphqzm9zrf
    '';
    region = "ap-northeast";

    file_path = "\${resource.local_sensitive_file.paparimo-age-img.filename}";
    file_hash = "\${resource.local_sensitive_file.paparimo-age-img.content_md5}";
  };

  resource.linode_image.paparimo-img = {
    label = "paparimo-img";
    region = "ap-northeast";

    file_path = "${inputs.nixos-config.packages.x86_64-linux.paparimo}/nixos.img.gz";
    file_hash = "\${filemd5(\"${inputs.nixos-config.packages.x86_64-linux.paparimo}/nixos.img.gz\")}";
  };

  # Create a boot disk
  resource.linode_instance_disk.paparimo-root = {
    label = "paparimo-root";
    linode_id = "\${linode_instance.paparimo.id}";
    size = 6072;
    image = "\${linode_image.paparimo-img.id}";
  };

  # Create a swap disk
  resource.linode_instance_disk.paparimo-swap = {
    label = "paparimo-swap";
    linode_id = "\${linode_instance.paparimo.id}";
    size = 8092;
    filesystem = "swap";
  };

  # Create a age disk
  resource.linode_instance_disk.paparimo-age = {
    label = "paparimo-age";
    linode_id = "\${linode_instance.paparimo.id}";
    size = 1;
    image = "\${linode_image.paparimo-age-img.id}";
  };

  resource.linode_instance_disk.paparimo-persist = {
    label = "paparimo-persist";
    linode_id = "\${linode_instance.paparimo.id}";
    size = "\${linode_instance.paparimo.specs.0.disk - 6072 - 8092 - 3 - 2048}";
    filesystem = "ext4";
  };

  resource.linode_instance_disk.paparimo-persist-var-log = {
    label = "paparimo-persist-var-log";
    linode_id = "\${linode_instance.paparimo.id}";
    size = 2048;
    filesystem = "ext4";
  };

  resource.linode_instance_config.paparimo-main = {
    linode_id = "\${linode_instance.paparimo.id}";
    label = "paparimo-main";
    booted = true;
    kernel = "linode/grub2";
    root_device = "/dev/sda";

    devices = {
      sda.disk_id = "\${linode_instance_disk.paparimo-root.id}";
      sdb.disk_id = "\${linode_instance_disk.paparimo-swap.id}";
      sdc.disk_id = "\${linode_instance_disk.paparimo-age.id}";
      sdd.disk_id = "\${linode_instance_disk.paparimo-persist.id}";
      sde.disk_id = "\${linode_instance_disk.paparimo-persist-var-log.id}";
    };
  };

  resource.linode_instance.paparimo = {
    label = "paparimo";
    type = "g6-standard-4";
    region = "ap-south";
  };
}
