{ inputs }:

{
  data.sops_file.yasyutora-age-img = {
    source_file = "${./yasyutora-age.linode.img.gz.base64}";
    input_type = "raw";
  };

  resource.local_sensitive_file.yasyutora-age-img = {
    content_base64 = "\${data.sops_file.yasyutora-age-img.raw}";
    filename = "\${path.module}/.secrets/yasyutora.age.linode.img.gz";
  };

  resource.linode_image.yasyutora-age-img = {
    label = "yasyutora-age-img";
    description = ''
      Image contain the age key for sops.
      Public key: age1jl8w8tap0revg0glkmedy7vku6rht0hkuda7mz20uh5gleu7w5mqjlra4z
    '';
    region = "ap-northeast";

    file_path = "\${resource.local_sensitive_file.yasyutora-age-img.filename}";
    file_hash = "\${resource.local_sensitive_file.yasyutora-age-img.content_md5}";
  };

  resource.linode_image.yasyutora-img = {
    label = "yasyutora-img";
    region = "ap-northeast";

    file_path = "${inputs.nixos-config.packages.x86_64-linux.yasyutora}/nixos.img.gz";
    file_hash = "\${filemd5(\"${inputs.nixos-config.packages.x86_64-linux.yasyutora}/nixos.img.gz\")}";
  };

  # Create a boot disk
  resource.linode_instance_disk.yasyutora-root = {
    label = "yasyutora-root";
    linode_id = "\${linode_instance.yasyutora.id}";
    size = 6072;
    image = "\${linode_image.yasyutora-img.id}";
  };

  # Create a swap disk
  resource.linode_instance_disk.yasyutora-swap = {
    label = "yasyutora-swap";
    linode_id = "\${linode_instance.yasyutora.id}";
    size = 8092;
    filesystem = "swap";
  };

  # Create a age disk
  resource.linode_instance_disk.yasyutora-age = {
    label = "yasyutora-age";
    linode_id = "\${linode_instance.yasyutora.id}";
    size = 1;
    image = "\${linode_image.yasyutora-age-img.id}";
  };

  resource.linode_instance_disk.yasyutora-persist = {
    label = "yasyutora-persist";
    linode_id = "\${linode_instance.yasyutora.id}";
    size = "\${linode_instance.yasyutora.specs.0.disk - 6072 - 8092 - 1 - 2048}";
    filesystem = "ext4";
  };

  resource.linode_instance_disk.yasyutora-persist-var-log = {
    label = "yasyutora-persist-var-log";
    linode_id = "\${linode_instance.yasyutora.id}";
    size = 2048;
    filesystem = "ext4";
  };

  resource.linode_instance_config.yasyutora-main = {
    linode_id = "\${linode_instance.yasyutora.id}";
    label = "yasyutora-main";
    booted = true;
    kernel = "linode/grub2";
    root_device = "/dev/sda";

    devices = {
      sda.disk_id = "\${linode_instance_disk.yasyutora-root.id}";
      sdb.disk_id = "\${linode_instance_disk.yasyutora-swap.id}";
      sdc.disk_id = "\${linode_instance_disk.yasyutora-age.id}";
      sdd.disk_id = "\${linode_instance_disk.yasyutora-persist.id}";
      sde.disk_id = "\${linode_instance_disk.yasyutora-persist-var-log.id}";
    };
  };

  resource.linode_instance.yasyutora = {
    label = "yasyutora";
    type = "g6-standard-1";
    region = "ap-northeast";
  };
}
