{ flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  perSystem = { system, lib, pkgs, ... }:
    let
      terraform = pkgs.terraform.withPlugins (p: with p; [
        linode
        local
        sops
      ]);

      required_providers = {
        linode.source = "linode/linode";
        sops.source = "carlpett/sops";
      };

      wrappedTerraform = pkgs.writeShellScriptBin "terraform-linode" ''
        while IFS= read -r key && read -r value; do {
          declare -x "$key=$value";
        }; done < <(${pkgs.sops}/bin/sops -d ${./env.linode.json} \
          | ${pkgs.jq}/bin/jq -r 'to_entries[] | (.key, .value)')

        [ -d .terraform-linode ] || mkdir .terraform-linode

        cd .terraform-linode

        if [[ -e config.tf.json ]]; then rm -f config.tf.json; fi
        nix build ../#packages.${system}.linodeConfig -o config.tf.json \
          && ${terraform}/bin/terraform init -reconfigure \
          && ${terraform}/bin/terraform $@
      '';

      linodeModule = {
        terraform = {
          inherit required_providers;
          backend.s3 = {
            bucket = "samuelsung-terraform-state";
            key = "linode/terraform.tfstate";
            encrypt = true;
            skip_credentials_validation = true;
          };
        };

        provider.sops = { };
      };

      linodeConfig = inputs.terranix.lib.terranixConfiguration {
        inherit system;
        modules = [
          linodeModule
          (importApply ./paparimo { inherit inputs; })
          (importApply ./yasyutora { inherit inputs; })
        ];
      };
    in
    rec {
      devenv.shells.default.packages = [
        wrappedTerraform
      ];

      packages = { inherit linodeConfig; };
    };
}
