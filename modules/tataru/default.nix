{ flake-parts-lib, inputs, ... }:

let
  inherit (flake-parts-lib) importApply;
in
{
  perSystem = { system, lib, pkgs, ... }:
    let
      terraform = pkgs.terraform.withPlugins (p: with p; [
        local
        libvirt
        sops
      ]);

      required_providers = {
        libvirt.source = "dmacvicar/libvirt";
        sops.source = "carlpett/sops";
      };

      wrappedTerraform = pkgs.writeShellScriptBin "terraform-tataru" ''
        PATH=$PATH:${lib.makeBinPath [
          pkgs.libxslt
        ]}

        while IFS= read -r key && read -r value; do {
          declare -x "$key=$value";
        }; done < <(${pkgs.sops}/bin/sops -d ${./env.tataru.json} \
          | ${pkgs.jq}/bin/jq -r 'to_entries[] | (.key, .value)')

        [ -d .terraform-tataru ] || mkdir .terraform-tataru

        cd .terraform-tataru

        if [[ -e config.tf.json ]]; then rm -f config.tf.json; fi
        nix build ../#packages.${system}.tataruConfig -o config.tf.json \
          && ${terraform}/bin/terraform init -reconfigure \
          && ${terraform}/bin/terraform $@
      '';

      tataruModule = {
        terraform = {
          inherit required_providers;

          backend.local.path = "./terraform-sunanoie.tfstate";
        };

        provider.sops = { };
      };

      tataruConfig = inputs.terranix.lib.terranixConfiguration {
        inherit system;
        modules = [
          tataruModule
          ./networks.nix
          ./secrets.nix
          (importApply ./naar { inherit inputs; })
          (importApply ./sankureddo { inherit inputs; })
        ];
      };
    in
    {
      devenv.shells.default.packages = [
        wrappedTerraform
      ];

      packages = { inherit tataruConfig; };
    };
}
