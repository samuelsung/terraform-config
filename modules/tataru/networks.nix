{
  resource.libvirt_network.virnicbr0 = {
    name = "virnicbr0";
    mode = "bridge";
    bridge = "virnicbr0";
    autostart = true;
  };

  resource.libvirt_network.virnicbr1 = {
    name = "virnicbr1";
    mode = "bridge";
    bridge = "virnicbr1";
    autostart = true;
  };

  resource.libvirt_network.virnicbr2 = {
    name = "virnicbr2";
    mode = "bridge";
    bridge = "virnicbr2";
    autostart = true;
  };

  resource.libvirt_network.virnicbr3 = {
    name = "virnicbr3";
    mode = "bridge";
    bridge = "virnicbr3";
    autostart = true;
  };
}
