{ inputs }:

{ lib, ... }:

let
  inherit (lib) head splitString toUpper;
  getHash = result: head (splitString "-" (baseNameOf result.outPath));
  hostname = "naar";
  root = inputs.nixos-config.packages.x86_64-linux.${hostname};
  nixosConfiguration = inputs.nixos-config.nixosConfigurations.${hostname}.config;
in
{
  # public key: age1js4cdsk8s9zg7kt2fsxu7majv6cvfldvuj0u2f6yn5ffl83f3fnsc805an
  data.sops_file."${hostname}-key-iso" = {
    source_file = "${./.}/${hostname}.tataru.iso.base64";
    input_type = "raw";
  };

  resource.local_sensitive_file."${hostname}-key-iso" = {
    content_base64 = "\${ data.sops_file.${hostname}-key-iso.raw }";
    filename = "\${path.module}/.secrets/${hostname}-key.iso";
  };

  resource.libvirt_volume."${hostname}-key-iso" = {
    name = "${hostname}-key-iso";
    pool = "secrets";
    source = "\${ resource.local_sensitive_file.${hostname}-key-iso.filename }";
    depends_on = [ "resource.libvirt_pool.secrets" ];
  };

  resource.libvirt_volume."${hostname}-root-base" = {
    name = "${hostname}-root-base";
    source = "${root}/nixos.qcow2";
  };

  resource.libvirt_volume."${hostname}-root" = {
    name = "${hostname}-root";
    size = 20 * 1024 * 1024 * 1024;
    base_volume_id = "\${ resource.libvirt_volume.${hostname}-root-base.id }";
  };

  resource.libvirt_volume."persist-base" = {
    name = "persist-base";
    source = "${../persist-base.qcow2}";
  };

  resource.libvirt_volume."${hostname}-persist" = {
    name = "${hostname}-persist";
    size = 10 * 1024 * 1024 * 1024;
    base_volume_id = "\${ resource.libvirt_volume.persist-base.id }";
  };

  resource.libvirt_volume."persist-var-log-base" = {
    name = "persist-var-log-base";
    source = "${../persist-var-log.qcow2}";
  };

  resource.libvirt_volume."${hostname}-persist-var-log" = {
    name = "${hostname}-persist-var-log";
    size = 5 * 1024 * 1024 * 1024;
    base_volume_id = "\${ resource.libvirt_volume.persist-var-log-base.id }";
  };

  resource.libvirt_domain.${hostname} = {
    name = hostname;
    description = ''
      deps:
      secrets-iso: ''${ sha256( data.sops_file.${hostname}-key-iso.raw ) }
      rpool: ${getHash root}}
    '';
    cpu.mode = "host-passthrough";
    memory = 3072;
    vcpu = 4;
    running = true;

    disk = [
      { volume_id = "\${ libvirt_volume.${hostname}-root.id }"; }
      { volume_id = "\${ libvirt_volume.${hostname}-key-iso.id }"; }
      { volume_id = "\${ libvirt_volume.${hostname}-persist.id }"; }
      { volume_id = "\${ libvirt_volume.${hostname}-persist-var-log.id }"; }
    ];

    arch = "x86_64";
    qemu_agent = true;

    network_interface = [
      {
        bridge = "\${ libvirt_network.virnicbr0.bridge }";
        mac = toUpper nixosConfiguration.networking.fix-interfaces.lan0;
        wait_for_lease = false;
      }
      {
        bridge = "\${ libvirt_network.virnicbr1.bridge }";
        mac = toUpper nixosConfiguration.networking.fix-interfaces.wan0;
        wait_for_lease = false;
      }
      {
        bridge = "\${ libvirt_network.virnicbr2.bridge }";
        mac = toUpper nixosConfiguration.networking.fix-interfaces.opt0;
        wait_for_lease = false;
      }
      {
        bridge = "\${ libvirt_network.virnicbr3.bridge }";
        mac = toUpper nixosConfiguration.networking.fix-interfaces.opt1;
        wait_for_lease = false;
      }
    ];

    console = {
      type = "pty";
      target_port = "0";
      target_type = "serial";
    };
  };
}
