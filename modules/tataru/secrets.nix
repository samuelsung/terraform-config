{
  resource.libvirt_pool.secrets = {
    name = "secrets";
    type = "dir";
    path = "/var/lib/libvirt/secret-images";
    xml.xslt = ''
      <?xml version="1.0"?>
      <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        <xsl:template match="pool">
          <pool type='dir'>
            <name><xsl:value-of select="name"/></name>
            <target>
              <path><xsl:value-of select="target/path"/></path>
              <permissions>
                <mode>0600</mode>
              </permissions>
            </target>
          </pool>
        </xsl:template>
      </xsl:stylesheet>
    '';
  };
}
