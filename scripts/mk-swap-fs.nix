{ coreutils
, lib
, libguestfs-with-appliance
, makeWrapper
, qemu-utils
, stdenv
}:

stdenv.mkDerivation rec {
  name = "mk-swap-fs";

  nativeBuildInputs = [
    makeWrapper
  ];

  src = ./.;

  buildPhase = "";

  installPhase = ''
    mkdir -p $out/bin
    cp mk-swap-fs.bash $out/bin/${name}
    chmod +x $out/bin/${name}

    wrapProgram $out/bin/${name} \
      --prefix PATH : ${lib.makeBinPath [
        coreutils
        qemu-utils
        libguestfs-with-appliance
      ]}
  '';

  meta = with lib; {
    homepage = "https://codeberg.org/samuelsung/terraform-config";
    description = "terraform-config";
    license = licenses.mit;
    platforms = platforms.linux;
  };
}
