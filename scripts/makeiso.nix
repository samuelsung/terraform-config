{ coreutils
, lib
, libxslt
, makeWrapper
, stdenv
, xorriso
}:

stdenv.mkDerivation rec {
  name = "makeiso";

  nativeBuildInputs = [
    makeWrapper
  ];

  src = ./.;

  buildPhase = "";

  installPhase = ''
    mkdir -p $out/bin
    cp makeiso.bash $out/bin/${name}
    chmod +x $out/bin/${name}

    wrapProgram $out/bin/${name} \
      --prefix PATH : ${lib.makeBinPath [
        coreutils
        libxslt
        xorriso
      ]}
  '';

  meta = with lib; {
    homepage = "https://codeberg.org/samuelsung/terraform-config";
    description = "terraform-config";
    license = licenses.mit;
    platforms = platforms.linux;
  };
}
