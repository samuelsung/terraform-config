#!/usr/bin/env bash

set -eu -o pipefail

# Helper to just fail with a message and non-zero exit code.
function fail() {
  echo "$1" >&2
  exit 1
}

SOPS_PROFILE=$1
TMP_DIR=$(mktemp -d -t secret_img_XXXXXXXX)
IMAGE_MNT=$TMP_DIR/mnt
IMAGE_PATH=$TMP_DIR/secret.img
IMAGE_BASE64_PATH=$TMP_DIR/secret.${SOPS_PROFILE}.img.base64

# Helper to clean up after ourselves if we're killed by SIGINT.
function clean_up() {
  echo "Script killed, cleaning up tmpdirs: $TMP_DIR" >&2
  rm -rf "$TMP_DIR"
}

# clean up if we're killed.
trap clean_up SIGINT

umask 077

qemu-img create -f raw "$IMAGE_PATH" 200K >&2

guestfish <<EOF >&2
  add-drive $IMAGE_PATH
  run
  mkfs ext4 /dev/sda
EOF

mkdir "$IMAGE_MNT"
guestmount -w -a "$IMAGE_PATH" -m /dev/sda -o uid="$(id -u)" "$IMAGE_MNT"
age-keygen -o "$IMAGE_MNT/key.txt"
guestunmount "$IMAGE_MNT"

gzip "$IMAGE_PATH"
base64 "$IMAGE_PATH.gz" >"$IMAGE_BASE64_PATH"
sops -e "$IMAGE_BASE64_PATH"

rm -rf "$TMP_DIR"
