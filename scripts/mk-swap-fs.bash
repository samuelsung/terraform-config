#!/usr/bin/env bash

set -eu -o pipefail

# Helper to just fail with a message and non-zero exit code.
function fail() {
  echo "$1" >&2
  exit 1
}

TMP_DIR=$(mktemp -d -t img_XXXXXXXX)
IMAGE_PATH=$TMP_DIR/img.qcow2

# Helper to clean up after ourselves if we're killed by SIGINT.
function clean_up() {
  echo "Script killed, cleaning up tmpdirs: $TMP_DIR" >&2
  rm -rf "$TMP_DIR"
}

# clean up if we're killed.
trap clean_up SIGINT

umask 077

qemu-img create -f qcow2 "$IMAGE_PATH" 60K >&2

guestfish <<EOF >&2
  add-drive $IMAGE_PATH
  run
  mkswap /dev/sda
EOF

cp "$IMAGE_PATH" .

rm -rf "$TMP_DIR"
