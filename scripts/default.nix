{
  perSystem = { pkgs, ... }:
    let
      inherit (pkgs) callPackage;
    in
    {
      packages.makeiso = callPackage ./makeiso.nix { };
      packages.mk-age-image = callPackage ./mk-age-image.nix { };
      packages.mk-image = callPackage ./mk-image.nix { };
      packages.mk-swap-fs = callPackage ./mk-swap-fs.nix { };
    };
}
