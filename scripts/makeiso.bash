#!/usr/bin/env bash

set -eu -o pipefail

# Helper to just fail with a message and non-zero exit code.
function fail() {
  echo "$1" >&2
  exit 1
}

SOPS_PROFILE=$1
TMP_DIR=$(mktemp -d -t secret_iso_XXXXXXXX)
AGE_KEY_PATH=$TMP_DIR/key.txt
ISO_PATH=$TMP_DIR/secret.iso
ISO_BASE64_PATH=$TMP_DIR/secret.${SOPS_PROFILE}.iso.base64

# Helper to clean up after ourselves if we're killed by SIGINT.
function clean_up() {
  echo "Script killed, cleaning up tmpdirs: $TMP_DIR" >&2
  rm -rf "$TMP_DIR"
}

# clean up if we're killed.
trap clean_up SIGINT

umask 077

age-keygen >"$AGE_KEY_PATH"
chmod 0400 "$AGE_KEY_PATH"

xorriso -as mkisofs -o "$ISO_PATH" -uid 0 -graft-points -R key\.txt="$AGE_KEY_PATH" 1>&2

base64 "$ISO_PATH" >"$ISO_BASE64_PATH"

sops -e "$ISO_BASE64_PATH"

echo "age public key:" 1>&2
age-keygen -y "$AGE_KEY_PATH" 1>&2

rm -rf "$TMP_DIR"
