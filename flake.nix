{
  description = "terraform config for personal use";

  inputs = {
    commitlint-config.url = "git+https://codeberg.org/samuelsung/commitlint-config";
    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixos-config.url = "git+https://codeberg.org/samuelsung/nixos-config";
    nixpkgs.url = "nixpkgs/nixos-23.05";
    terranix.url = "github:terranix/terranix";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; }
      {
        imports = [
          inputs.devenv.flakeModule
          ./modules/linode/default.nix
          ./modules/tataru/default.nix
          ./scripts
        ];
        systems = [ "x86_64-linux" ];
        perSystem = { system, ... }:
          {
            devenv.shells.default = {
              pre-commit.hooks = {
                deadnix.enable = true;
                nixpkgs-fmt.enable = true;
                statix.enable = true;

                commitlint = inputs.commitlint-config.hook.${system};
              };
            };
          };
      };
}
