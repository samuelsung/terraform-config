# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/carlpett/sops" {
  version = "0.7.2"
  hashes = [
    "h1:yB65Qn3aWA+47Qq8IFJKc4b4AOc7BuoLSBDL3rzJXno=",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.4.0"
  hashes = [
    "h1:1jqG0T+YiS6AOo34e3fLo3Tlje3eRVF82+P09BXhVr8=",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version = "2.2.0"
  hashes = [
    "h1:/fFFxDLsmYk7ZTHnyxJe+73MVU2fJ04liFQ7j/Kd6qI=",
  ]
}
