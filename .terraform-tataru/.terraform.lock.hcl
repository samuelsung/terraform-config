# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/carlpett/sops" {
  version = "0.7.2"
  hashes = [
    "h1:yB65Qn3aWA+47Qq8IFJKc4b4AOc7BuoLSBDL3rzJXno=",
  ]
}

provider "registry.terraform.io/dmacvicar/libvirt" {
  version = "0.7.1"
  hashes = [
    "h1:2e1jeovt4Pxf04iTEy3tGKYM6beQ6Mvx/k0YNa6kIUU=",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.4.0"
  hashes = [
    "h1:1jqG0T+YiS6AOo34e3fLo3Tlje3eRVF82+P09BXhVr8=",
  ]
}
